"use strict";

// Global variables
let epicImages = [];
let selectedImageType = "";

(function() {
	
	const EpicApplication = {
		epicImages: [],
		selectImageType: "",
		dateCache: {},
		imageCache: {
			aerosol: new Map(),
			cloud: new Map(),
			enhanced: new Map(),
			natural: new Map()
		},
		imageTypeSet: new Set(["aerosol", "cloud", "enhanced", "natural"])
	};
	
	const imageTypes = ["aerosol", "cloud", "enhanced", "natural"];
	
    document.addEventListener("DOMContentLoaded", function() {

        const requestForm = document.getElementById('request_form');
        const imageMenu = document.getElementById('image-menu');
        const earthImage = document.getElementById('earth-image');
        const imageDate = document.getElementById('earth-image-date');
        const imageCaption = document.getElementById('earth-image-title');

        requestForm.addEventListener('submit', function (e) {
            e.preventDefault();

            selectedImageType = document.getElementById('type').value;
            var date = document.getElementById('date').value;

            if (date) {
                // Fetch data for a specific date
                fetchImageDataForDate(date);
            } else {
                // Fetch data for all dates
                fetchAllImageData();
            }
        });

        fetchAllImageData(); // Fetch data for the initial rendering

        function fetchAllImageData() {
			
			if(EpicApplication.dateCache[selectedImageType]) {
				fetchImageDataForDate(EpicApplication.dateCache[selectedImageType]);
				return;
			}
			
			selectedImageType = document.getElementById('type').value;
			var apiURL = 'https://epic.gsfc.nasa.gov/api/' + selectedImageType + '/all';

			fetch(apiURL)
				.then(response => {
					if (!response.ok) {
						throw new Error('Response was not ok');
					}
					return response.json();
				})
				.then(data => {
					epicImages = data;

					if (data.length > 0) {
						// Map each date to a new Date()
						const dateObjects = data.map(item => new Date(item.date));

						// Sort the dates in descending order
						dateObjects.sort((a, b) => b - a);

						// Retrieve the first index (most recent date)
						const mostRecentDate = dateObjects[0];

						// Convert the Date object to a string of the format yyyy-mm-dd
						const formattedDate = formatDateToString(mostRecentDate);
						
						// update the cache
						EpicApplication.dateCache[selectedImageType] = formattedDate;
						EpicApplication.imageCache[selectedImageType].set(formattedDate, epicImages);
						// Debugg line to check if the most recent data is correct
						console.log('Most recent date:', formattedDate);

						// Update the menu with only the most recent date
						updateImageMenu([{ date: formattedDate }]);
					} 
					else {
						showErrorMessage('No data available.');
					}
				})
				.catch(error => {
					console.error('Error trying to fetch data: ', error);
					showErrorMessage('Error fetching data.');
				});
		}


        function fetchImageDataForDate(date) {
            var apiURL = 'https://epic.gsfc.nasa.gov/api/' + selectedImageType + '/date/' + date;

            fetch(apiURL)
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Response was not ok');
                    }
                    return response.json();
                })
                .then(data => {
                    epicImages = data;
                    updateImageMenu(epicImages);
                })
                .catch(error => {
                    console.error('Error trying to fetch data for the specified date: ', error);
                    showErrorMessage('Error fetching data for the specified date.');
                });
        }

        function formatDateToString(date) {
            const year = date.getFullYear();
            const month = (date.getMonth() + 1).toString().padStart(2, '0');
            const day = date.getDate().toString().padStart(2, '0');
            return `${year}-${month}-${day}`;
        }

        function showErrorMessage(message) {
            imageMenu.innerHTML = `<li>${message}</li>`;
            earthImage.innerHTML = ''; 
            imageDate.textContent = '';
            imageCaption.textContent = '';
        }

        // Function to update the image menu
        function updateImageMenu(images) {
			imageMenu.innerHTML = '';
			if (Array.isArray(images)) {
				images.forEach((image, index) => {
					const menuItem = createMenuItem(image, index);
					imageMenu.appendChild(menuItem);
				});
			} 
			else {
				const menuItem = createMenuItem(images, 0);
				imageMenu.appendChild(menuItem);
			}
		}

		function createMenuItem(image, index) {
			
			const template = document.getElementById('image-menu-item');
			const menuItem = template.content.cloneNode(true).querySelector('li');
			const spanElement = menuItem.querySelector('span');
			spanElement.textContent = `${image.date}`;
			menuItem.setAttribute('data-index', index);
			menuItem.addEventListener('click', function () {
				const clickedIndex = parseInt(menuItem.getAttribute('data-index'));
				displayImage(clickedIndex);
			});
			imageMenu.appendChild(menuItem);
			return menuItem;
		}

        // Function to display image details
        function displayImageDetails(index = 0) {
            const selectedImage = epicImages[index];
            imageDate.textContent = `${selectedImage.date}`;
            imageCaption.textContent = `${selectedImage.caption}`;
        }

        // Function to display the selected image
        function displayImage(index) {
            const selectedImage = epicImages[index];
            const [year, month, day] = selectedImage.date.split(' ')[0].split('-');
            earthImage.src = `https://epic.gsfc.nasa.gov/archive/${selectedImageType}/${year}/${month}/${day}/jpg/${selectedImage.image}.jpg`;

            // Display image details
            displayImageDetails(index);
        }
    });
})();
